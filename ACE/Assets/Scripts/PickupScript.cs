﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupScript : MonoBehaviour {

    public HealthManager health;
    public AudioSource playeraudiosource;
    public AudioClip heart;

    // Use this for initialization
    void Start () {

        if (GameObject.FindGameObjectWithTag("Manager") != null)
        {
            health = GameObject.FindGameObjectWithTag("Manager").GetComponent<HealthManager>();
        }

        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            playeraudiosource = GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>();
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            playeraudiosource.PlayOneShot(heart, 0.7F);
            health.Playerhealth = health.Playerhealth + 1;
            Destroy(this.gameObject);
        }
    }
}

