﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boxscript : MonoBehaviour
{

    public int boxhealth;
    private bool isStunned;
    public GameObject hitparticle;
    public GameObject dropitem;
    public bool candrop;
    public bool canemit;
    public BoxCollider playerweapon;
    private Rigidbody boxBody;
    public GameObject player;
    public float stuntime;

    // Use this for initialization
    void Start()
    {
        boxBody = GetComponent<Rigidbody>();
        candrop = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (boxhealth <= 0)
        {

            if (candrop == true)
            {
                Instantiate(dropitem,
                new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, this.gameObject.transform.position.z),
                this.gameObject.transform.rotation);

                candrop = false;
            }
        }


    }
    IEnumerator Stun()
    {
        if (player.transform.position.x > this.transform.position.x)
        {
            boxBody.AddForce(-40, 100, 0);
        }
        else
        {
            boxBody.AddForce(40, 100, 0);
        }
        yield return new WaitForSeconds(stuntime);
        isStunned = false;
    }

        public void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Playerweapon" && isStunned == false)
        {
            isStunned = true;
            boxhealth = boxhealth - 1;
            GameObject particle = Instantiate(hitparticle,
            new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, this.gameObject.transform.position.z),
            this.gameObject.transform.rotation);
            particle.transform.parent = null;
            StartCoroutine(Stun());
        }


        }
}


        
