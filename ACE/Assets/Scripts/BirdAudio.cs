﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class BirdAudio : MonoBehaviour {

    public AudioClip birdchirp;
    public AudioSource audioSource;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OneshotBirdchirp()
    {
        audioSource.PlayOneShot(birdchirp, 0.7F);
    }
}
