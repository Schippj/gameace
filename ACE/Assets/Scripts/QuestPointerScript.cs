﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestPointerScript : MonoBehaviour
{
    private GameObject target;
    private Vector3 targetPoint;
    private Quaternion targetRotation;

    void Start()
    {
        target = GameObject.FindWithTag("Quest");
    }

    void Update()
    {
        transform.LookAt (new Vector3(target.transform.position.x,transform.position.y, target.transform.position.z));
    }
}