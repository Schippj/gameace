﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnterZoneScript : MonoBehaviour {

    public GameObject sign;
    public Text Area;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Area")
        {
            sign.GetComponent<Animator>().SetTrigger("zonesignTrigger");
            Area.text = other.GetComponent<ZoneName>().AreaName;
            sign.GetComponentInChildren<Text>().text = Area.text;
        }

    }
}
