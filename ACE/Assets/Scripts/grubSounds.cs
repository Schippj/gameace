﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class grubSounds : MonoBehaviour
{

    public AudioClip TakeDamage;
    public AudioClip Die;


    public AudioSource audioSource;

    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {

    }
    void OneshotTakeDamage()
    {
        audioSource.PlayOneShot(TakeDamage, 0.7F);
    }
    void OneshotDie()
    {
        audioSource.PlayOneShot(Die, 0.7F);
    }
}