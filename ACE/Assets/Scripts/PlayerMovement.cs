﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{


    private Animator animator;
    public float movementSpeed;
    private Rigidbody acerigid;
    public float jumpheight;
    public bool IsGrounded;
    public bool CanAirAttack;
    public float BackstepAmount;
    private float Backstep;
    [SerializeField]
    private HealthManager healthmanager;
    public List<Image> Hearts = new List<Image>();
    public float deathtimer;
    public bool Isdead;
    public GameObject killparticle;
    public bool CanEmit;


    // Use this for initialization
    void Start()
    {
        CanEmit = true;
        animator = GetComponentInChildren<Animator>();
        acerigid = GetComponent<Rigidbody>();
        Backstep = BackstepAmount;
        if (GameObject.FindGameObjectWithTag("Manager") != null)
        {
            healthmanager = GameObject.FindGameObjectWithTag("Manager").GetComponent<HealthManager>();
        }
        int temp = 0;
        foreach (Image image in Hearts)
        {
            temp++;
            if (temp <= healthmanager.Playerhealth)
            {
                image.enabled = true;
            }
            else
            {
                image.enabled = false;
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        healthHud();
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.W))
        {
            if (IsGrounded)
            {
                animator.SetBool("Walk", true);
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(new Vector3(-movementSpeed, 0, 0));
                transform.localScale = new Vector3(-1, 1, 1);
                Backstep = -BackstepAmount;
            }
            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(new Vector3(0, 0, movementSpeed));
            }
        }

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.W))
        {
            if (IsGrounded)
            {
                animator.SetBool("Walk", true);
            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(new Vector3(movementSpeed, 0, 0));
                transform.localScale = new Vector3(1, 1, 1);
                Backstep = BackstepAmount;
            }
            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(new Vector3(0, 0, movementSpeed));
            }

        }

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S))
        {
            if (IsGrounded)
            {
                animator.SetBool("Walk", true);
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(new Vector3(-movementSpeed, 0, 0));
                transform.localScale = new Vector3(-1, 1, 1);
                Backstep = -BackstepAmount;
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(new Vector3(0, 0, -movementSpeed));
            }

        }

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.S))
        {
            if (IsGrounded)
            {
                animator.SetBool("Walk", true);
            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(new Vector3(movementSpeed, 0, 0));
                transform.localScale = new Vector3(1, 1, 1);
                Backstep = BackstepAmount;
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(new Vector3(0, 0, -movementSpeed));
            }

            //jumping                
        }
        else if (Input.anyKey == false) { animator.SetBool("Walk", false); }
        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded)
        {
            acerigid.AddForce(new Vector3(0, jumpheight, 0), ForceMode.Impulse);
            if (!IsGrounded) { }
        }
        //attack states
        if (Input.GetMouseButtonDown(0))
        {
            animator.SetTrigger("AttackA");
        }
        if (Input.GetMouseButtonDown(1))
        {
            animator.SetTrigger("AttackB");
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (IsGrounded == false && CanAirAttack == true)
            {
                acerigid.AddForce(new Vector3(Backstep, jumpheight / 8 * 2, 0), ForceMode.Impulse);
                animator.SetTrigger("AttackA");
                CanAirAttack = false;

            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            if (IsGrounded == false && CanAirAttack == true)
            {
                acerigid.AddForce(new Vector3(0, -6, 0), ForceMode.Impulse);
                animator.SetTrigger("AttackB");
                CanAirAttack = false;

            }
        }

        if (healthmanager.Playerhealth <= 0 && Isdead == false)
        {
            StartCoroutine(Death());
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            print("ishit");
            animator.SetTrigger("Hurt");
            acerigid.AddForce(new Vector3(-Backstep, 0, 0), ForceMode.Impulse);
            healthmanager.Playerhealth = healthmanager.Playerhealth - 1;
        }
    }
    //check ground collision
    void OnCollisionStay(Collision collisionInfo)
    {
        if (collisionInfo.gameObject.tag == "Ground")
        {
            IsGrounded = true;
            animator.SetBool("Airborne", false);
            CanAirAttack = true;
        }

    }

    void OnCollisionExit(Collision collisionInfo)
    {
        if (collisionInfo.gameObject.tag == "Ground")
        {
            IsGrounded = false;
            animator.SetBool("Airborne", true);
            animator.SetBool("Walk", false);
        }

    }

    void healthHud()
    {
        int i = 1;
        foreach (Image heart in Hearts)
        {
            if (healthmanager.Playerhealth < i)
            {
                heart.enabled = false;
            }
            if (healthmanager.Playerhealth >= i)
            {
                heart.enabled = true;
            }
            i++;
        }
    }
    IEnumerator Death()
    {
        if (CanEmit == true)
        {
            GameObject particle = Instantiate(killparticle,
               new Vector3(this.gameObject.transform.position.x, 0.3f, this.gameObject.transform.position.z),
               this.gameObject.transform.rotation);
            particle.transform.parent = null;
            CanEmit = false;
            movementSpeed = 0;
        Isdead = true;
        animator.SetBool("Death", true);
        acerigid.AddForce(new Vector3(-Backstep, 1, 0), ForceMode.Impulse);
        Time.timeScale = 0.2F;
        yield return new WaitForSeconds(deathtimer);
        Time.timeScale = 1F;
        animator.SetTrigger("Respawn");
        

        }
    }
}