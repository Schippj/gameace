﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestAScript : MonoBehaviour {
    public GameObject QuestMarker;
    public GameObject[] Objective;
    public int current;

    private void Start()
    {
        QuestMarker.transform.position = Objective[0].transform.position;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            current++;
            QuestMarker.transform.position = Objective[current].transform.position;
        }
    }
}