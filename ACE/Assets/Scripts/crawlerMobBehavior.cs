﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class crawlerMobBehavior : MonoBehaviour {

    public Collider playerBox;
    public Collider monsterSight;
    public Collider monsterHit;
    public Transform target;
    public float movementSpeed;
    public BoxCollider playerweapon;
    private Rigidbody crawlerBody;
    public int stuntime;
    private bool wait;
    public GameObject player;
    public float step;
    public Animator animator;
    public int Mobhealth;
    private float currentDist;
    public float Dist;
    private bool isStunned;
    public GameObject killparticle;
    public GameObject dropheart;
    public bool candrop;

    // Use this for initialization
    void Start () {
        crawlerBody = GetComponent<Rigidbody>();
        candrop = true;
	}

    // Update is called once per frame
    void Update() {
        currentDist = Vector3.Distance(player.transform.position, this.transform.position);
        step = movementSpeed * Time.deltaTime;
        if (Mobhealth <= 0)
        {
            GameObject particle = Instantiate(killparticle,
                new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, this.gameObject.transform.position.z),
                this.gameObject.transform.rotation);
            particle.transform.parent = null;
            if (candrop == true)
            {
                Instantiate(dropheart,
                new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, this.gameObject.transform.position.z),
                this.gameObject.transform.rotation);
                particle.transform.parent = null;
                candrop = false;
            }
                
            animator.SetTrigger("death");


        }
        if (player.transform.position.x > this.transform.position.x)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
  
        if (wait == false)
        {
            if (currentDist < Dist)
            {
                if (Checkrange())
                {
                    animator.SetBool("walking", false);
                    transform.position = Vector3.MoveTowards(transform.position, target.position, 0);
                }
                else
                {
                    animator.SetBool("walking", true);
                    transform.position = Vector3.MoveTowards(transform.position, target.position, step);
                }
                
            }

        }
        
    }
    IEnumerator Stun()
    {
        
        if (player.transform.position.x > this.transform.position.x)
            {
            animator.SetBool("stagger",true);
            crawlerBody.AddForce(-40, 100, 0);
        }
        else
        {
            animator.SetBool("stagger",true);
            crawlerBody.AddForce(40, 100, 0);
        }
        yield return new WaitForSeconds(stuntime);
        animator.SetBool("stagger", false);
        wait = false;
        isStunned = false;
    }
   
    public bool Checkrange()
    {

        float dist = Vector3.Distance(target.position, transform.position);
        if (dist <= 0.4f)
        {
            return true;
        }
        return false;
        
    }

    public void MobDeath()
    {
        Destroy(this.gameObject);
    }

    public void OnTriggerEnter(Collider collision)
    {

        if (collision.gameObject.tag == "Playerweapon" && isStunned == false)
        {
            isStunned = true;
            wait = true;
            Mobhealth = Mobhealth - 1;
            StartCoroutine(Stun());
        }
            
        
    }

}
